# Написать функцию которая принимает числа, выводит сумму чисел. Функцию надо вызвать

def summa(num1, num2, num3):
    print(num1 + num2 + num3)

summa(78, 59, 55)

mass = list(range(1, 100))

def one(mass):
    dcfg = 0
    for two in mass:
        dcfg += two
    print(dcfg)

one(mass)



# Написать функцию которая принимает числа, выводит разность чисел. Функцию надо вызвать.

def raznost(one, two, three):
    print(one - two -three)

raznost(454, 652, 515)


# Написать функцию которая принимает числа, выводит произведение чисел. Функцию надо вызвать.

def proizvedenie(four, five, six):
    print(four * five * six)

proizvedenie(25,45,12)

# Написать функцию которая принимает числа, выводит деление чисел. Функцию надо вызвать.

def delenie(seven, eight, nine):
    print(seven / eight / nine)

delenie(888, 56, 69)


# Написать функцию которая принемает массив, проходится по циклом по массиву и печатает объекты массива. Функцию надо вызвать.

world = [{
    'gorod': 'Bishkek',
    'strana': 'Kyrgyzstan',
    'location': 'Central Asia',
},
{
    'gorod2': 'Balykchy',
    'strana2': 'Kyrgyzstan',
    'location3': 'Central Asia',
    }]


def print_df(array):
    for i in array:
        print(i)

print_df(world)



# Написать функцию которая принемает массив, проходится по циклом по массиву и печатает объекты массива. Функцию надо вызвать.

chisla = [{
    'odin': 125,
    'dva': 556,
    'tri': 548,
    'chetyre': 489,
}]

def sifry(test):
    for mars in test:
        print(mars)

sifry(chisla)


# Напишите приммеры использования всех операций с массивами
arr = ['a','b','c','d','e', 'Meder']
arr_3 = ['j', 'k', 'l']

# len(' ')

print(len(arr))

 # append()

arr.append('f')
print(arr)

 # clear()

# arr.clear()
# print(arr)

 # count()

print(arr.count('a'))

 # copy()

arr_2 = arr.copy()
print(arr_2)

 # extend()

arr.extend(arr_3)
print(arr)

# index()
print(arr.index('Meder'))

# remove('Meder')
arr.remove('Meder')
print(arr)

# reverse()
arr.reverse()
print(arr)

# pop()
arr.pop(3)
print(arr)


# Оберните все операции в функции, которые принимают масссив и выполняют над нимм операцию. Функцию надо вызвать.
arr_4 = ['a','b','c','d','e', 'Meder']

# len()

def show_len_array(array):
    print('Длинна массива - ', len(array))

show_len_array(arr)

    # append()

def show_append_array(array2):
    array2.append('f')
    print(array2)

show_append_array(arr_4)

    # clear()
# def show_clear_array(array3):
#     array3.clear()
#     print(array3)
#
# show_clear_array(arr_4)

    # count()
def show_count_array(array4):
    print(array4.count('b'))

show_count_array(arr_4)

    # copy()
def show_copy_array(array5):
    array_6 = array5.copy()
    print(array_6, arr_4)

show_copy_array(arr_4)

    # extend()
def show_extend_array(array7):
    array7.extend(arr_3)
    print(array7)

show_extend_array(arr_4)

    # index()
def show_index_array(array8):
    print(array8.index('Meder'))

show_index_array(arr_4)

    # remove()
def show_remove_array(array9):
    array9.remove('Meder')
    print(array9)
show_remove_array(arr_4)

    # reverse()
def show_reverse_array(array10):
    array10.reverse()
    print(array10)
show_reverse_array(arr_4)

    # pop()
def show_pop_array(array11):
    array11.pop(1)
    print(array11)
show_pop_array(arr_4)

