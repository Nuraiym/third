# Вставляйте решение под задачей

# К просмотре обязательно ---- https://www.youtube.com/watch?v=zZBiln_2FhM

# Создайте массив состоящий из 5ти словарей

GDP = [{
    'Country': 'Kyrgyzstan',
    'Population': 7000000,
    'location': 'Central Asia',
    'GDP': '8 bln$'
},
    {
    'Country': 'Kazahstan',
    'Population': 19000000,
    'location': 'Central Asia',
    'GDP': '181.7 bln $'
},
    {
    'Country': 'Russia',
    'Population': 145000000,
    'location': 'Central Asia',
    'GDP': '1.7 trln $',
},
    {
    'Country': 'Azerbaijan',
    'Population': 10200000,
    'location': 'Central Asia',
    'GDP': '48 bln $'
    },
    {
    'Country': 'Uzbekistan',
    'Population': 34000000,
    'location': 'Central Asia',
    'GDP': '58 bln $'
    }
]

# Создайте массив состоящий из 5ти массивов с любыми значениями
mov = [1, 2, 5, 8, 84, [4, 5, 'hello', ['hello world', 'python', [1, 2, [3, 5], 'html'], 'C++'], 'java'], 6, 7]
print(mov)

# Замените в ранее созданном массиве несколько значений
mov = [1, 2, 5, 8, 84, [4, 5, 'world', ['kyrgyzstan', 'Australia', [1, 2, [3, 5], 'html'], 'C++'], 'java'], 6, 7]


# Создайте цикл для первого масссива
for country in mov:
    print(country)

# Создайте цикл для второго масссива
for country_2 in mov[5]:
    print(country_2)

# Создайте массив состоящий из 10 цифр от 0 и до 20
number = [12, 18, 10, 9, 5, 15, 20, 19, 2, 1]


# Пройдитесь циклоом по вышесозданному муссиву и выведите все чита которые больше 10ти
for ten in number:
    if ten >= 10 :
        print(ten)

# Создайте словарь который содержит в себе все типы данных которые мы проходили (ключи на ваше усморение)
joint = {
    'nubmer': 125635,
    'book': ['voina i mir', 'five'],
    'all': ['numbers', 'books', 'countries', 125, 515]
}
print(joint)


# Создайте словарь который содержит в себе 5 разных массивов
legend = {
    'name': ['Nuraiym', 'Aigerim', 'Bekaiym'],
    'study': ['KRSU', 'Ataturk', 'KRSU'],
    'country': ['Kyrgyzstan', 'Australia', 'USA', 'Turkey'],
    'book': [1, 2, 3, 4, 5],
    'phone': ['Iphone 10', 'Iphone 11', 'Iphone 12']
}


# Создайте условия if / else
if legend['phone'] == 'Iphone 10':
    print('   best   ')
else:
    print('   the best  ')


# Создайте условия if / elif / else
legend_2 = {
    'name': ['Nuraiym', 'Aigerim', 'Bekaiym'],
    'study': ['KRSU', 'Ataturk', 'KRSU'],
    'country': ['Kyrgyzstan', 'Australia', 'USA', 'Turkey'],
    'book': [1, 2, 3, 4, 5],
    'phone': ['Iphone 10', 'Iphone 11', 'Iphone 12'],
    'Sardarbekova': True
}
if legend_2['phone'] == 'Iphone 10':
    print('   best   ')
elif legend_2['Sardarbekova']:
    print('   best   ')
else:
    print('   the best  ')


# Создайте условия if / elif / elif / else
legend_3 = {
    'name': ['Nuraiym', 'Aigerim', 'Bekaiym'],
    'study': ['KRSU', 'Ataturk', 'KRSU'],
    'country': ['Kyrgyzstan', 'Australia', 'USA', 'Turkey'],
    'book': [1, 2, 3, 4, 5],
    'phone': ['Iphone 10', 'Iphone 11', 'Iphone 12'],
    'Sardarbekova': False,
    'Shamshybaeva': True
}

if legend_3['phone'] == 'Iphone':
    print('   best   ')
elif legend_3['Sardarbekova']:
    print('   best   ')
elif legend_3['Shamshybaeva']:
    print('   best   ')
else:
    print('   the best  ')


# Создайте условия цикл который проходится по массиву в который содержит в себе условия if / elif / else
new = [{
    'name': ['Nursultan', 'Aisultan', 'Kanat'],
    'study': ['KRSU', 'Ataturk', 'KRSU'],
    'country': ['Kyrgyzstan', 'Australia', 'USA', 'Turkey'],
    'book': [1, 2, 3],
    'phone': ['Iphone 10', 'Iphone 11', 'Iphone 12'],
    'Sardarbekov': True,
    'Akylbekov': True
},
    {
    'name': ['Nursultan', 'Aisultan', 'Kanat'],
    'study': ['KRSU', 'Ataturk', 'KRSU'],
    'country': ['Kyrgyzstan', 'Australia', 'USA', 'Turkey'],
    'book': [1, 2, 3],
    'phone': ['Iphone 10', 'Iphone 11', 'Iphone 12'],
    'Sardarbekov': False,
    'Akylbekov': True
    }]

for new_2 in new:
    if new_2['phone'] == 'Iphone 10':
        print('   best   ')
    elif new_2['Sardarbekov']:
        print('   best   ')
    elif new_2['Akylbekov']:
        print('   best   ')
    else:
        print('   the best  ')

